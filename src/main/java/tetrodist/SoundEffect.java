package tetrodist;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import tetrodist.asset.ProviderImage;
import tetrodist.event.EventBus;

public class SoundEffect {
	protected EventBus bus;
	protected Player move;
	protected ExecutorService executor;
	protected Map<Integer, String> seTable;

	public SoundEffect(EventBus bus) {
		this.bus = bus;
		try {
			InputStream in = ProviderImage.class.getResourceAsStream("move.mp3");
			this.move = new Player(in);
		} catch (JavaLayerException e) {
			e.printStackTrace();
		}
		
		this.seTable = new HashMap<Integer, String>();
		this.seTable.put(EventBus.MOVE_LEFT, "move.mp3");
		this.seTable.put(EventBus.MOVE_RIGHT, "move.mp3");
		this.seTable.put(EventBus.ROTATE_LEFT, "roll.mp3");
		this.seTable.put(EventBus.ROTATE_RIGHT, "roll.mp3");
		this.seTable.put(EventBus.TETROMINO_FIXED, "land.mp3");
		
		this.executor = Executors.newCachedThreadPool();
	}

	public void listen() {
		final Map<Integer, String> seTable = this.seTable;
		final ExecutorService executor = this.executor;

		this.bus.add(ActionListener.class, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (seTable.containsKey(e.getID())) {
					final String name = seTable.get(e.getID());
					executor.submit(new Runnable() {
						@Override
						public void run() {
							try {
								InputStream in = ProviderImage.class.getResourceAsStream(name);
								Player player = new Player(in);
								player.play();
							} catch (JavaLayerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
				}
			}
		});
	}
}
