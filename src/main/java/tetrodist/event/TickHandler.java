package tetrodist.event;

import java.awt.AWTEvent;

public class TickHandler implements EventHandler {
	
	protected EventBus bus;

	public TickHandler(EventBus bus) {
		this.bus = bus;
	}
	
	public TickListener[] getTickListeners() {
		return this.bus.getListeners(TickListener.class);
	}

	@Override
	public void processEvent(AWTEvent e) {
		if (e instanceof TickEvent) {
			this.processTickEvent((TickEvent)e);
		}
	}
	
	public void processTickEvent(TickEvent e) {
		for (TickListener l : this.getTickListeners()) {
			l.tick((TickEvent)e);
		}
	}
	
	@Override
	public Class<?> getEventClass() {
		return TickEvent.class;
	}
}
