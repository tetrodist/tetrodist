package tetrodist.event;

import java.awt.AWTEvent;

public interface EventHandler {
	public void processEvent(AWTEvent e);
	public Class<?> getEventClass();
}
