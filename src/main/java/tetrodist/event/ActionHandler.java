package tetrodist.event;

import java.awt.AWTEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionHandler implements EventHandler {
	
	protected EventBus bus;

	public ActionHandler(EventBus bus) {
		this.bus = bus;
	}
	
	public ActionListener[] getActionListeners() {
		return this.bus.getListeners(ActionListener.class);
	}

	@Override
	public void processEvent(AWTEvent e) {
		if (e instanceof ActionEvent) {
			this.processActionEvent((ActionEvent)e);
		}
	}
	
	public void processActionEvent(ActionEvent e) {
		for (ActionListener l : this.getActionListeners()) {
			l.actionPerformed((ActionEvent)e);
		}
	}
	
	@Override
	public Class<?> getEventClass() {
		return ActionEvent.class;
	}
}
