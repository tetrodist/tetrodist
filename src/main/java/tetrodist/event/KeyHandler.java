package tetrodist.event;

import java.awt.AWTEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements EventHandler {
	
	protected EventBus bus;

	public KeyHandler(EventBus bus) {
		this.bus = bus;
	}
	
	public KeyListener[] getKeyListeners() {
		return this.bus.getListeners(KeyListener.class);
	}

	@Override
	public void processEvent(AWTEvent e) {
		if (e instanceof KeyEvent) {
			this.processKeyEvent((KeyEvent)e);
		}
	}
	
	public void processKeyEvent(KeyEvent e) {
		switch (e.getID()) {
		case KeyEvent.KEY_PRESSED:
			this.processKeyPressed(e);
			break;
		case KeyEvent.KEY_RELEASED:
			this.processKeyReleased(e);
			break;
		case KeyEvent.KEY_TYPED:
			this.processKeyTyped(e);
			break;
		}
	}
	
	public void processKeyPressed(KeyEvent e) {
		for (KeyListener l : this.getKeyListeners()) {
			l.keyPressed((KeyEvent)e);
		}
	}
	
	public void processKeyReleased(KeyEvent e) {
		for (KeyListener l : this.getKeyListeners()) {
			l.keyReleased((KeyEvent)e);
		}
	}
	
	public void processKeyTyped(KeyEvent e) {
		for (KeyListener l : this.getKeyListeners()) {
			l.keyTyped((KeyEvent)e);
		}
	}
	
	@Override
	public Class<?> getEventClass() {
		return KeyEvent.class;
	}
}
