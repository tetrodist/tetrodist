package tetrodist.event;

import java.util.EventListener;

public interface TickListener extends EventListener {
	public void tick(TickEvent e);
}
