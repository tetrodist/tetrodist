package tetrodist.event;

import java.awt.AWTEvent;
import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.event.EventListenerList;

public class EventBus extends EventListenerList implements Closeable {
	private static final long serialVersionUID = 1L;

	public static final int GRAVITY = AWTEvent.RESERVED_ID_MAX + 10;
	public static final int TICK = AWTEvent.RESERVED_ID_MAX + 20;
	public static final int EVENT_CHANGE = AWTEvent.RESERVED_ID_MAX + 30;
	public static final int REQUEST = AWTEvent.RESERVED_ID_MAX + 40;

	public static final int MOVE_LEFT = AWTEvent.RESERVED_ID_MAX + 50;
	public static final int MOVE_RIGHT = AWTEvent.RESERVED_ID_MAX + 60;
	public static final int ROTATE_LEFT = AWTEvent.RESERVED_ID_MAX + 70;
	public static final int ROTATE_RIGHT = AWTEvent.RESERVED_ID_MAX + 80;

	public static final int TETROMINO_FIXING = AWTEvent.RESERVED_ID_MAX + 90;
	public static final int TETROMINO_FIXED = AWTEvent.RESERVED_ID_MAX + 100;

	public static final int PAINT = AWTEvent.RESERVED_ID_MAX + 110;

	public static final int FALL = AWTEvent.RESERVED_ID_MAX + 120;

	public static final int INJECTING = AWTEvent.RESERVED_ID_MAX + 130;
	public static final int INJECTED = AWTEvent.RESERVED_ID_MAX + 140;

	public static final int ERASE_SINGLE = AWTEvent.RESERVED_ID_MAX + 200;
	public static final int ERASE_DOUBLE = AWTEvent.RESERVED_ID_MAX + 210;
	public static final int ERASE_TRIPLE = AWTEvent.RESERVED_ID_MAX + 220;
	public static final int ERASE_TETRIS = AWTEvent.RESERVED_ID_MAX + 230;
	
	public static final int GAMEOVER = AWTEvent.RESERVED_ID_MAX + 300;

	protected BlockingQueue<AWTEvent> eventQueue;
	protected EventListenerList listeners;
	
	protected Map<Class<?>, EventHandler> handlers;
	protected CompletionService<Void> appCompletion;

	private ExecutorService executor;
	protected Logger logger;

	public EventBus() {
		this.listeners = new EventListenerList();
		this.eventQueue = new ArrayBlockingQueue<AWTEvent>(10);
		this.handlers = new HashMap<Class<?>, EventHandler>();
		
		this.executor = Executors.newSingleThreadExecutor();
		this.logger = Logger.getGlobal();
	}

	public void postEvent(AWTEvent e) {
		eventQueue.offer(e);
	}
	
	public void putEventHandler(Class<?> eventClass, EventHandler h) {
		handlers.put(eventClass, h);
	}
	
	public void removeEventHandler(EventHandler h) {
		handlers.remove(h.getEventClass());
	}

	public void processEvent(AWTEvent e) {
		EventHandler h = this.handlers.get(e.getClass());
		if (h != null) {
			h.processEvent(e);
		}
	}

	public void open() {
		final BlockingQueue<AWTEvent> eventQueue = this.eventQueue;

		this.executor.execute(new Runnable() {
			@Override
			public void run() {
				while (!Thread.interrupted()) {
					AWTEvent e;
					try {
						e = eventQueue.take();
					} catch (InterruptedException ie) {
						return;
					}

					try {
						processEvent(e);
					} catch (RuntimeException re) {
						logger.logp(Level.WARNING, "EventBus", "processEvent", "Invalidated Event", re);
					}
				}
			}
		});
	}
	
	@Override
	public void close() throws IOException {
		this.executor.shutdownNow();
	}
}
