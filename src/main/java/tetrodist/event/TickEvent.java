package tetrodist.event;

import java.awt.AWTEvent;

public class TickEvent extends AWTEvent {
	private static final long serialVersionUID = -1L;
	
	public TickEvent(Object source) {
		super(source, EventBus.TICK);
	}
}
