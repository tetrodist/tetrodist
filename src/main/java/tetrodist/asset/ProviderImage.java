package tetrodist.asset;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ProviderImage {
	public static final String TYPE1 = "block1.png";
	public static final String TYPE2 = "block2.png";
	public static final String TYPE3 = "block3.png";
	public static final String TYPE4 = "block4.png";
	public static final String AROUND = "around.png";

	private Map<Integer, ImageIcon> icons;
	private Map<String, BufferedImage> images;
	private int type;
	
	public ProviderImage() {
		this.icons = new HashMap<>();
		this.images = new HashMap<>();
	}

	public ImageIcon getBlockIcon(int type) {
		ImageIcon icon = this.icons.get(type);
		if (icon == null) {
			BufferedImage img = this.getBufferedImage("Iblock" + type);
			icon = new ImageIcon(img);
			this.icons.put(type, icon);
		}
		return icon;
	}
	
	public BufferedImage getBlockImage(String name) {
		return this.getBufferedImage(name + "block" + type);
	}

	public BufferedImage getTetrominoImage(String name) {
		return this.getBufferedImage(name + "mino" + type);
	}
	
	public BufferedImage getBufferedImage(String name) {
		BufferedImage img = this.images.get(name);
		if (img == null) {
			img = this.newImage(name + ".png");
			this.images.put(name, img);
		}
		return img;
	}
	
	public int getType() {
		return this.type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public BufferedImage newImage(String name) {
		InputStream input = ProviderImage.class.getResourceAsStream(name);

		try {
			BufferedImage img = ImageIO.read(input);
			return img;
		} catch (IOException e) {
			return this.getMissing();
		} catch (IllegalArgumentException e) {
			return this.getMissing();
		}
	}

	public BufferedImage getMissing() {
		BufferedImage img = new BufferedImage(16, 16,
				BufferedImage.TYPE_INT_RGB);

		Graphics g = img.getGraphics();
		g.setColor(Color.RED);
		g.drawString("?", 6, 11);
		g.dispose();

		return img;
	}
}
