package tetrodist;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import tetrodist.activity.ActivityClient;
import tetrodist.activity.ActivityEntrance;
import tetrodist.activity.ActivityServer;
import tetrodist.asset.ProviderImage;
import tetrodist.discovery.Advertise;
import tetrodist.event.ActionHandler;
import tetrodist.event.EventBus;
import tetrodist.event.KeyHandler;
import tetrodist.event.TickEvent;
import tetrodist.event.TickHandler;
import tetrodist.event.TickListener;
import tetrodist.field.KeyInputTranslator;

public class Main {
	public static void main(String[] args) {
		Logger.getGlobal().setLevel(Level.WARNING);
		new Main().start();
	}

	protected ExecutorService control;
	protected CompletionService<Void> appCompletion;
	protected MainWindow window;
	protected Ticker ticker;
	protected EventBus globalBus;
	protected ServerSocket ss;

	protected Advertise advertise;
	
	public Main() {
		this.control = Executors.newCachedThreadPool();
		this.appCompletion = new ExecutorCompletionService<Void>(this.control);

		this.window = new MainWindow();
		this.globalBus = new EventBus();
		this.ticker = new Ticker(this.appCompletion);
	}
	
	public void start() {
		ActivityEntrance actSetting = new ActivityEntrance(this.window);
		actSetting.open();

		GameEnv env;
		try {
			env = actSetting.queryGameEnv();
		} catch (InterruptedException e) {
			return;
		} finally {
			try {
				actSetting.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		this.ticker.addTickListener(new TickListener() {
			@Override
			public void tick(TickEvent e) {
				Main.this.globalBus.postEvent(e);
			}
		});
		
		this.window.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				Main.this.globalBus.postEvent(e);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				Main.this.globalBus.postEvent(e);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				Main.this.globalBus.postEvent(e);
			}
		});

		this.globalBus.putEventHandler(ActionEvent.class, new ActionHandler(this.globalBus));
		this.globalBus.putEventHandler(TickEvent.class, new TickHandler(this.globalBus));
		this.globalBus.putEventHandler(KeyEvent.class, new KeyHandler(this.globalBus));

		this.ticker.start();
		this.globalBus.open();

		if (env.server) {
			this.startServer(env);
		} else {
			this.startClient(env);
		}
		
		try {
			while (true) {
				Future<Void> future = this.appCompletion.take();
				future.get();
			}
		} catch (InterruptedException e) {
			System.out.println("exiting main");
		} catch (ExecutionException e) {
			e.printStackTrace();
			throw (RuntimeException)e.getCause();
		}
	}
	
	private void startServer(GameEnv env) {
		ProviderImage pi = new ProviderImage();
		pi.setType(env.blockType);

		ActivityServer actGame = new ActivityServer(window, globalBus, pi);
		actGame.open(env.name);
	}
	
	private void startClient(GameEnv env) {
		KeyInputTranslator key = new KeyInputTranslator(globalBus);
		key.registerRepeatKey(KeyEvent.VK_LEFT, EventBus.MOVE_LEFT);
		key.registerRepeatKey(KeyEvent.VK_RIGHT, EventBus.MOVE_RIGHT);
		//key.registerOneshotKey(KeyEvent.VK_UP, EventBus.FALL);
		key.registerRepeatKey(KeyEvent.VK_DOWN, EventBus.GRAVITY);
		key.registerOneshotKey(KeyEvent.VK_Z, EventBus.ROTATE_LEFT);
		key.registerOneshotKey(KeyEvent.VK_X, EventBus.ROTATE_RIGHT);

		ProviderImage pi = new ProviderImage();
		pi.setType(4);


		ActivityClient act = new ActivityClient(window, globalBus, pi);
		act.open(env.name);
		JabberClient client = new JabberClient(globalBus, act.ctrl.field);
		try {
			InetAddress addr = InetAddress.getByName(env.hostname);
			client.open(addr, env.port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
