package tetrodist.comm;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class BaseComm implements Closeable {
	protected final ServerSocket serverSocket;
	protected final ExecutorService executor;
	protected Logger logger;
	
	public BaseComm(ServerSocket ss) {
		this.serverSocket = ss;
		this.executor = Executors.newCachedThreadPool();
		
		this.logger = Logger.getGlobal();
	}
	
	public void open() throws IOException {
		executor.execute(new Runnable() {
			List<Socket> sockets = new ArrayList<>();

			@Override
			public void run() {
				try {
					while (!Thread.interrupted()) {
						Socket s;
						try {
							s = serverSocket.accept();
						} catch (SocketException e) {
							return;
						} catch (IOException e) {
							logger.logp(Level.WARNING, "BaseComm", "open", "ServerSocket", e);
							return;
						}

						sockets.add(s);
						openConnection(s);
					}
				} finally {
					for (Socket s : sockets) {
						forceClose(s);
					}
				}
			}
		});
	}
	
	@Override
	public void close() {
		forceClose(serverSocket);
		executor.shutdownNow();
	}
	
	public ServerSocket getServerSocket() {
		return serverSocket;
	}
	
	protected abstract void openConnection(Socket s);

	protected void forceClose(Closeable c) {
		try {
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
