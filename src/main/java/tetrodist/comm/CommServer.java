package tetrodist.comm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.msgpack.MessagePack;

import tetrodist.event.EventBus;
import tetrodist.field.Field;

public class CommServer extends BaseComm {
	private final EventBus bus;
	private final Map<String, Integer> eventIds;
	private final Field field;

	public CommServer(ServerSocket ss, EventBus bus, Field field) {
		super(ss);
		
		this.bus = bus;
		this.field = field;
		
		this.eventIds = new HashMap<>();
		eventIds.put("z", EventBus.ROTATE_LEFT);
		eventIds.put("x", EventBus.ROTATE_RIGHT);
		eventIds.put(",", EventBus.MOVE_LEFT);
		eventIds.put(".", EventBus.GRAVITY);
		eventIds.put("/", EventBus.MOVE_RIGHT);
	}

	@Override
	protected void openConnection(final Socket s) {
		InputStream in;
		final OutputStream out;
		try {
			in = s.getInputStream();
			out = s.getOutputStream();
		} catch (IOException e) {
			logger.logp(Level.WARNING, "CommServer", "openConnection", "can't connect", e);
			return;
		}
		InputStreamReader inr = new InputStreamReader(in);
		final BufferedReader br = new BufferedReader(inr);
		
		OutputStreamWriter outw = new OutputStreamWriter(out);
		BufferedWriter bw = new BufferedWriter(outw);

		final MessagePack msgpack = new MessagePack();
		bus.add(ActionListener.class, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getID() == EventBus.EVENT_CHANGE) {
					List<List<Integer>> rows = new ArrayList<>();
					for (int i = 0; i < field.getHeight(); i++) {
						List<Integer> cols = new ArrayList<>();
						rows.add(cols);
						for (int j = 0; j < field.getWidth(); j++) {
							cols.add(field.getColorAt(i, j).getRGB());
						}
					}
					try {
						byte[] buf = msgpack.write(rows);
						out.write(buf);
					} catch (IOException ex) {
					}
				}
			}
		});

		executor.execute(new Runnable() {
			@Override
			public void run() {
				if (logger.isLoggable(Level.INFO)) {
					logger.info("Sterted: " + s);
					logger.info("Connection accepted: " + s);
				}

				while (!Thread.interrupted()) {
					String str;
					try {
						str = br.readLine();
					} catch (SocketException e) {
						if (logger.isLoggable(Level.INFO)) {
							logger.logp(Level.INFO, "CommServer", "openConnection", "disconnect", e);
						}
						return;
					} catch (IOException e) {
						logger.logp(Level.WARNING, "CommServer", "openConnection", "exception", e);
						e.printStackTrace();
						return;
					}

					if (str == null) {
						if (logger.isLoggable(Level.INFO)) {
							logger.logp(Level.INFO, "CommServer", "openConnection", "disconnected by remote");
						}
						return;
					}
					
					Integer id = eventIds.get(str);
					if (id != null) {
						bus.postEvent(new ActionEvent(this, id, null));
					}
				}
			}
		});
	}
}
