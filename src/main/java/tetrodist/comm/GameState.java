package tetrodist.comm;

import java.util.List;

import org.msgpack.annotation.Message;

@Message
public class GameState {
	public int score;
	public int tetrominoNext;
	public List<List<Integer>> fields;
}
