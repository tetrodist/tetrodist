package tetrodist.view;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import tetrodist.asset.ProviderImage;
import tetrodist.event.EventBus;
import tetrodist.field.Scorer;

public class PaintScore implements ActionListener {
	protected Scorer scorer;
	public AffineTransform transform = new AffineTransform();
	public int digitWidth;
	protected PaintDispatcher bus;

	private HashMap<Integer, BufferedImage> digitImages;
	
	public PaintScore(PaintDispatcher bus, Scorer scorer) {
		this.bus = bus;
		ProviderImage pi = new ProviderImage();
		this.scorer = scorer;
		
		this.digitImages = new HashMap<Integer, BufferedImage>();
		for (int i = 0; i < 10; i++) {
			this.digitImages.put(i, pi.newImage(i + ".png"));
		}
	}
	
	public void start() {
		this.bus.addActionListener(this);
	}

	public void paint(PaintSession session) {
		Graphics2D g = session.getGraphics();
		int score = scorer.getScore();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < digitWidth; i++) {
			sb.append("0");
		}
		sb.append(score);

		char[] digits = sb.substring(sb.length() - digitWidth).toCharArray();
		
		g.setTransform(transform);
		AffineTransform xform = new AffineTransform();
		for (char d : digits) {
			BufferedImage img = this.digitImages.get(d - '0');
			g.drawRenderedImage(img, xform);
			xform.translate(img.getWidth(), 0.0);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getID() == EventBus.PAINT) {
			this.paint((PaintSession) e.getSource());
		}
	}
}
