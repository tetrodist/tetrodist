package tetrodist.view;

import java.awt.Graphics2D;
import java.awt.Window;

public class PaintSession {
	protected Graphics2D graphics;
	protected Window window;
	
	public PaintSession(Graphics2D g, Window w) {
		this.graphics = g;
		this.window = w;
	}
	
	public Graphics2D getGraphics() {
		return graphics;
	}
	
	public Window getWindow() {
		return window;
	}
}
