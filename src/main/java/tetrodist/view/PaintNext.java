package tetrodist.view;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.IOException;

import tetrodist.asset.ProviderImage;
import tetrodist.event.EventBus;
import tetrodist.field.TetroQueue;
import tetrodist.field.Tetromino;
import tetrodist.field.TetrominoI;
import tetrodist.field.TetrominoJ;
import tetrodist.field.TetrominoL;
import tetrodist.field.TetrominoO;
import tetrodist.field.TetrominoS;
import tetrodist.field.TetrominoT;
import tetrodist.field.TetrominoZ;

public class PaintNext implements ActionListener, Closeable {
	protected PaintDispatcher bus;
	protected TetroQueue queue;
	protected ProviderImage pi;
	protected int x;
	protected int y;
	public AffineTransform transform = new AffineTransform();

	public PaintNext(PaintDispatcher bus, TetroQueue queue, ProviderImage pi) {
		this.bus = bus;
		this.pi = pi;
		this.queue = queue;
	}
	
	public void setDimension(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void start() {
		this.bus.addActionListener(this);
	}

	@Override
	public void close() throws IOException {
		this.bus.removeActionListener(this);
	}

	public void paint(PaintSession session) {
		Graphics2D g = session.getGraphics();
		
		Tetromino mino = queue.peekFirst();
		BufferedImage imgMino;
		if (mino instanceof TetrominoI) {
			imgMino = this.pi.getTetrominoImage("I");
		} else if (mino instanceof TetrominoJ) {
			imgMino = this.pi.getTetrominoImage("J");
		} else if (mino instanceof TetrominoL) {
			imgMino = this.pi.getTetrominoImage("L");
		} else if (mino instanceof TetrominoO) {
			imgMino = this.pi.getTetrominoImage("O");
		} else if (mino instanceof TetrominoS) {
			imgMino = this.pi.getTetrominoImage("S");
		} else if (mino instanceof TetrominoT) {
			imgMino = this.pi.getTetrominoImage("T");
		} else if (mino instanceof TetrominoZ) {
			imgMino = this.pi.getTetrominoImage("Z");
		} else {
			return;
		}

		BufferedImage imgNext = pi.getBufferedImage("next");

		g.setTransform(transform);
		AffineTransform xform = new AffineTransform();
		g.drawRenderedImage(imgNext, xform);
		int diffx = imgNext.getWidth() - imgMino.getWidth();
		int diffy = imgNext.getHeight() - imgMino.getHeight();
		xform.translate(diffx / 2, diffy / 2);
		g.drawRenderedImage(imgMino, xform);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getID() == EventBus.PAINT) {
			this.paint((PaintSession) e.getSource());
		}
	}
}
