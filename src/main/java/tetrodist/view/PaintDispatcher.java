package tetrodist.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;

import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

import tetrodist.event.EventBus;
import tetrodist.field.Field;

public class PaintDispatcher implements ActionListener {
	protected Window window;
	protected Field field;
	protected BufferStrategy bs;
	protected EventListenerList listeners;
	protected boolean gameover;

	public PaintDispatcher(EventBus bus, Window window) {
		bus.add(ActionListener.class, this);
		this.window = window;
		this.listeners = new EventListenerList();
	}
	
	public void paint() {
		Window w = this.window;
		final BufferStrategy bs = w.getBufferStrategy();
		Graphics2D g = (Graphics2D)bs.getDrawGraphics();
		PaintSession session = new PaintSession(g, w);

		ActionEvent e = new ActionEvent(session, EventBus.PAINT, null);
		for (ActionListener l : this.getActionListeners()) {
			l.actionPerformed(e);
		}
		if (this.gameover) {
			g.setTransform(((Graphics2D)window.getGraphics()).getTransform());
			g.setColor(new Color(0, 0, 0, 128));
			g.fillRect(0,0,w.getWidth(),w.getHeight());
			AffineTransform xform = ((Graphics2D)window.getGraphics()).getTransform();
			xform.translate(80, 200);
			xform.scale(3, 3);
			g.setTransform(xform);
			g.setColor(Color.WHITE);
			g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
			g.drawString("GAME OVER", 0, 0);
		}
		g.dispose();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				bs.show();
			}
		});
	}

	public void addActionListener(ActionListener l) {
		this.listeners.add(ActionListener.class, l);
	}

	public void removeActionListener(ActionListener l) {
		this.listeners.remove(ActionListener.class, l);
	}

	public ActionListener[] getActionListeners() {
		return this.listeners.getListeners(ActionListener.class);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getID() == EventBus.EVENT_CHANGE) {
			paint();
		} else if (e.getID() == EventBus.GAMEOVER) {
			this.gameover = true;
			paint();
		}
	}
}
