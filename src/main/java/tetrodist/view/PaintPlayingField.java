package tetrodist.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import tetrodist.asset.ProviderImage;
import tetrodist.event.EventBus;
import tetrodist.field.Field;

public class PaintPlayingField implements ActionListener {
	public AffineTransform transform;

	private Field field;
	protected Image img1;
	protected Image img2;
	protected Image img3;
	protected Image img4;
	protected PaintDispatcher bus;

	private HashMap<Color, BufferedImage> blockImages;
	
	public PaintPlayingField(PaintDispatcher bus, Field field, ProviderImage pi) {
		this.bus = bus;
		this.field = field;
		this.blockImages = new HashMap<Color, BufferedImage>();
		this.blockImages.put(Color.BLUE, pi.getBlockImage("I"));
		this.blockImages.put(Color.WHITE, pi.newImage(ProviderImage.AROUND));
		this.blockImages.put(Color.YELLOW, pi.getBlockImage("O"));
		this.blockImages.put(Color.RED, pi.getBlockImage("Z"));
		this.blockImages.put(Color.MAGENTA, pi.getBlockImage("T"));
		this.blockImages.put(Color.CYAN, pi.getBlockImage("J"));
		this.blockImages.put(Color.ORANGE, pi.getBlockImage("L"));
		this.blockImages.put(new Color(0x9acd32), pi.getBlockImage("S"));
		this.blockImages.put(Color.BLACK, pi.newImage("back.png"));
	}

	public void paint(PaintSession session) {
		Graphics2D g = session.getGraphics();
		Field field = this.field;
		int height = field.getHeight();
		int width = field.getWidth();
		HashMap<Color, BufferedImage> blockImages = this.blockImages;

		g.setTransform(transform);
		AffineTransform xform = new AffineTransform();
		for (int row = -1; row <= height; row++) {
			xform.setToIdentity();
			xform.translate(0.0, 16.0 * (row + 1));
			for (int col = -1; col <= width; col++) {
				Color c = field.getColorAt(row, col);

				BufferedImage img = blockImages.get(c);
				g.drawRenderedImage(img, xform);
				xform.translate(16.0, 0.0);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getID() == EventBus.PAINT) {
			this.paint((PaintSession)e.getSource());
		}
	}
}
