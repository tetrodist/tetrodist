package tetrodist.controller;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import tetrodist.event.ActionHandler;
import tetrodist.event.EventBus;
import tetrodist.event.KeyHandler;
import tetrodist.event.TickEvent;
import tetrodist.event.TickHandler;
import tetrodist.event.TickListener;

abstract public class BaseController {
	public final EventBus localBus;
	public final Window window;

	public BaseController(EventBus globalBus, Window w) {
		this.localBus = createLocalBus(globalBus);
		this.window = w;
	}
	
	public EventBus getLocalBus() {
		return localBus;
	}
	
	public Window getWindow() {
		return window;
	}
	
	private EventBus createLocalBus(final EventBus globalBus) {
		final EventBus localBus = new EventBus();
	
		localBus.putEventHandler(ActionEvent.class, new ActionHandler(localBus));
		localBus.putEventHandler(TickEvent.class, new TickHandler(localBus));
		localBus.putEventHandler(KeyEvent.class, new KeyHandler(localBus));
		
		globalBus.add(ActionListener.class, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				localBus.postEvent(e);
			}
		});
	
		globalBus.add(TickListener.class, new TickListener() {
			@Override
			public void tick(TickEvent e) {
				localBus.postEvent(e);
			}
		});
		
		globalBus.add(KeyListener.class, new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				localBus.postEvent(e);
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				localBus.postEvent(e);
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				localBus.postEvent(e);
			}
		});
		
		return localBus;
	}
}