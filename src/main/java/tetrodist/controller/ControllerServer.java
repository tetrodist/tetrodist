package tetrodist.controller;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import tetrodist.SoundEffect;
import tetrodist.asset.ProviderImage;
import tetrodist.event.EventBus;
import tetrodist.field.Field;
import tetrodist.field.Gravity;
import tetrodist.field.KeyInputTranslator;
import tetrodist.field.LineEraser;
import tetrodist.field.Scorer;
import tetrodist.field.TetroFixer;
import tetrodist.field.TetroInjector;
import tetrodist.field.TetroQueue;
import tetrodist.field.TetrominoFactory;

public class ControllerServer extends BaseController {
	public final Field field;
	public final Gravity gravity;
	public final TetroQueue queue;
	public final TetroInjector injector;
	public final TetroFixer fixer;
	public final LineEraser eraser;
	public final TetrominoFactory factory;
	public final KeyInputTranslator key;
	public final Scorer scorer;

	public ControllerServer(EventBus globalBus, Window w, ProviderImage pi) {
		super(globalBus, w);

		this.field = new Field(localBus, 20, 10);
		this.gravity = new Gravity(localBus);
		this.factory = new TetrominoFactory(localBus, field);
		this.queue = new TetroQueue(factory);
		this.injector = new TetroInjector(localBus, field, queue);
		this.fixer = new TetroFixer(localBus, field);
		this.eraser = new LineEraser(localBus, field);
		this.key = createKeyInputTranslator(localBus);
		this.scorer = new Scorer(localBus);
	}
	
	public Field getField() {
		return field;
	}
	
	public Scorer getScorer() {
		return scorer;
	}

	public void start() {
		localBus.add(ActionListener.class, gravity);
		injector.open();
		localBus.add(ActionListener.class, fixer);
		localBus.add(ActionListener.class, eraser);
		localBus.add(ActionListener.class, scorer);
		localBus.open();

		SoundEffect se = new SoundEffect(getLocalBus());
		se.listen();
		
		getLocalBus().postEvent(new ActionEvent(this, EventBus.INJECTING, null));
	}
	
	private KeyInputTranslator createKeyInputTranslator(EventBus localBus) {
		KeyInputTranslator key = new KeyInputTranslator(localBus);
		key.registerRepeatKey(KeyEvent.VK_LEFT, EventBus.MOVE_LEFT);
		key.registerRepeatKey(KeyEvent.VK_RIGHT, EventBus.MOVE_RIGHT);
		key.registerOneshotKey(KeyEvent.VK_UP, EventBus.FALL);
		key.registerRepeatKey(KeyEvent.VK_DOWN, EventBus.GRAVITY);
		key.registerOneshotKey(KeyEvent.VK_Z, EventBus.ROTATE_LEFT);
		key.registerOneshotKey(KeyEvent.VK_X, EventBus.ROTATE_RIGHT);
		return key;
	}
}
