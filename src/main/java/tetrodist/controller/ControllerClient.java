package tetrodist.controller;

import java.awt.Window;

import tetrodist.event.EventBus;
import tetrodist.field.Field;

public class ControllerClient extends BaseController {
	public final Field field;

	public ControllerClient(EventBus globalBus, Window w) {
		super(globalBus, w);
		this.field = new Field(localBus, 20, 10);
	}
	
	public Field getField() {
		return field;
	}
}
