package tetrodist;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.event.EventListenerList;

import tetrodist.event.TickEvent;
import tetrodist.event.TickListener;

public class Ticker {
	protected EventListenerList listeners;
	protected ScheduledExecutorService scheduler;
	protected CompletionService<Void> appCompletion;

	public Ticker(CompletionService<Void> appCompletion) {
		this.listeners = new EventListenerList();
		this.appCompletion = appCompletion;
	}
	
	public void addTickListener(TickListener l) {
		this.listeners.add(TickListener.class, l);
	}
	
	public void removeTickListener(TickListener l) {
		this.listeners.remove(TickListener.class, l);
	}
	
	public TickListener[] getTickListeners() {
		return this.listeners.getListeners(TickListener.class);
	}

	public void start() {
		final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

		this.appCompletion.submit(new Runnable() {
			@Override
			public void run() {
				ScheduledFuture<?> f = scheduler.scheduleAtFixedRate(new Runnable() {
					@Override
					public void run() {
						TickEvent e = new TickEvent(this);
						for (TickListener l : Ticker.this.getTickListeners()) {
							l.tick(e);
						}
					}
				}, 0, 5, TimeUnit.MILLISECONDS);

				try {
					f.get();
				} catch (InterruptedException e) {
				} catch (ExecutionException e) {
					throw (RuntimeException)e.getCause();
				}
			}
		}, null);
	}
}
