package tetrodist.discovery;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.msgpack.MessageTypeException;

public class Solicit extends DiscoveryBase implements Closeable {
	public static final int PORT = 6835;
	public static final long AVERAGE_INTERVAL = 6000;
	public static final long MINIMUM_INTERVAL = 3000;
	public static final long EXPIRATION = 18000;

	private long delayOffset = Solicit.MINIMUM_INTERVAL;
	private long delayCoefficient = Solicit.AVERAGE_INTERVAL - Solicit.MINIMUM_INTERVAL;

	private ExecutorService executorReceiver;
	private ScheduledExecutorService executorSolicit;
	private ScheduledFuture<?> futureSolicit;
	private Selector selector;
	private DatagramChannel channel6;
	private DatagramChannel channel4;

	public DelayQueue<DiscoveryNodeDelayed> queue;
	public Map<DiscoveryNode,DiscoveryNodeDelayed> delayedNodes;

	public Solicit() throws IOException {
		this.executorReceiver = Executors.newSingleThreadExecutor();
		this.executorSolicit = Executors.newSingleThreadScheduledExecutor();
		this.queue = new DelayQueue<>();
		this.delayedNodes = new HashMap<>();

		this.selector = Selector.open();

		this.channel6 = this.openChennel(StandardProtocolFamily.INET6);
		this.channel6.bind(new InetSocketAddress(Solicit.PORT));
		this.channel6.register(this.selector, SelectionKey.OP_READ);

		this.channel4 = this.openChennel(StandardProtocolFamily.INET);
		this.channel4.bind(new InetSocketAddress(Solicit.PORT));
		this.channel4.register(this.selector, SelectionKey.OP_READ);
	}
	
	public void open() {
		this.rescheduleSolicit(0);

		this.executorReceiver.execute(new Runnable() {
			private ByteBuffer dst;

			@Override
			public void run() {
				this.dst = ByteBuffer.allocate(Advertise.DISCOVERY_SIZE);

				while (!Thread.interrupted()) {
					try {
						Solicit.this.selector.select();
					} catch (IOException e) {
						return;
					}
					
					if (Thread.interrupted()) {
						return;
					}

					Set<SelectionKey> keys = Solicit.this.selector.selectedKeys();
					Iterator<SelectionKey> iter = keys.iterator();
					while (iter.hasNext()) {
						SelectionKey key = iter.next();
						iter.remove();

						if (key.isReadable()) {
							this.handleReadable(key.channel());
						}
					}
				}
			}

			private void handleReadable(SelectableChannel sch) {
				try {
					ByteBuffer dst = ByteBuffer.allocate(Advertise.DISCOVERY_SIZE);
					DatagramChannel ch = (DatagramChannel)sch;
					InetSocketAddress remote = (InetSocketAddress)ch.receive(dst);
					DiscoveryMessage discmsg = Solicit.this.decodeMessage(dst);
				
					if (discmsg.advertise) {
						DiscoveryNode node = new DiscoveryNode(remote.getAddress(), discmsg.port, discmsg.name);
						Solicit.this.addNode(node);
						Solicit.this.rescheduleSolicit(Solicit.MINIMUM_INTERVAL);
					}
				} catch (IOException|ClassCastException|MessageTypeException e) {
					Solicit.this.throwing(e);
				}
			}
		});
	}
	
	public Set<DiscoveryNode> getNodes() {
		HashSet<DiscoveryNode> nodes = new HashSet<>();

		for (DiscoveryNodeDelayed delayed : this.queue) {
			if (delayed.getDelay(TimeUnit.MILLISECONDS) > 0) {
				nodes.add(delayed.getNode());
			}
		}
		
		return nodes;
	}
	
	private void addNode(DiscoveryNode node) {
		DiscoveryNodeDelayed delayed = this.delayedNodes.get(node);
		if (delayed == null) {
			delayed = new DiscoveryNodeDelayed(node, Solicit.EXPIRATION);
			this.delayedNodes.put(node, delayed);
			this.queue.add(delayed);
		}
		delayed.setDelay(Solicit.EXPIRATION);
	}

	private void rescheduleSolicit(long initialDelay) {
		if (this.futureSolicit != null) {
			this.futureSolicit.cancel(false);
		}

		final Random rng = new Random();
		final long offset = this.delayOffset;
		final long coeff = this.delayCoefficient;
		long delay = (long)(rng.nextDouble() * coeff) + offset + initialDelay;

		this.futureSolicit = this.executorSolicit.schedule(new Runnable() {
			@Override
			public void run() {
				Iterable<InetAddress> address6 = Solicit.this.getSolicitAddresses6();
				Iterable<InetAddress> address4 = Solicit.this.getSolicitAddresses4();

				Solicit.this.solicit(Solicit.this.channel6, address6);
				Solicit.this.solicit(Solicit.this.channel4, address4);
				
				long delay = (long)(rng.nextDouble() * coeff) + offset;
				Solicit.this.executorSolicit.schedule(this, delay, TimeUnit.MILLISECONDS);
			}
		}, delay, TimeUnit.MILLISECONDS);
	}

	private void solicit(DatagramChannel ch, Iterable<InetAddress> addresses) {
		ByteBuffer src;
		try {
			src = this.createMessageSolicit();
		} catch (IOException e) {
			this.throwing(e);
			return;
		}

		for (InetAddress address : addresses) {
			InetSocketAddress remote = new InetSocketAddress(address, Advertise.PORT);
			try {
				ch.send(src, remote);
				if (this.logger.isLoggable(Level.INFO)) {
					this.logger.info("Soliciting " + remote + "...");
				}
			} catch (IOException e) {
				this.throwing(e);
			}
		}
	}
	
	@Override
	public void close() throws IOException {
		this.executorReceiver.shutdownNow();
		this.executorSolicit.shutdown();
		if (this.logger.isLoggable(Level.INFO)) {
			this.logger.info("Solicit closed.");
		}
	}
}
