package tetrodist.discovery;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DiscoveryNodeDelayed implements Delayed {
	private final DiscoveryNode node;
	private long trigger;
	
	public DiscoveryNodeDelayed(DiscoveryNode node, long delayMillis) {
		this.node = node;
		this.setDelay(delayMillis);
	}
	
	public DiscoveryNode getNode() {
		return node;
	}
	
	public void setDelay(long delayMillis) {
		this.trigger = System.currentTimeMillis() + delayMillis;
	}
	
	@Override
	public long getDelay(TimeUnit unit) {
		long diff = this.trigger - System.currentTimeMillis();
		return unit.convert(diff, TimeUnit.MILLISECONDS);
	}

	@Override
	public int compareTo(Delayed o) {
		long x = this.getDelay(TimeUnit.MICROSECONDS);
		long y = o.getDelay(TimeUnit.MILLISECONDS);
		return Long.compare(x, y);
	}
}
