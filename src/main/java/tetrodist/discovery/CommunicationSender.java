package tetrodist.discovery;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class CommunicationSender implements Closeable {
	private OutputStream out;
	private OutputStreamWriter outWriter;
	private BufferedWriter outBufferedWriter;
	private PrintWriter outPrintWriter;

	public CommunicationSender(Socket s) throws IOException {
		this.out = s.getOutputStream();
		this.outWriter = new OutputStreamWriter(this.out);
		this.outBufferedWriter = new BufferedWriter(this.outWriter);
		this.outPrintWriter = new PrintWriter(this.outBufferedWriter, true);
	}
	
	public void send(String str) {
		this.outPrintWriter.println(str);
	}
	
	@Override
	public void close() {
		this.closeForce(this.outPrintWriter);
		this.closeForce(this.outWriter);
		this.closeForce(this.out);
	}

	private void closeForce(Closeable c) {
		try {
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
