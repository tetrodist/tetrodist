package tetrodist;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.msgpack.MessagePack;
import org.msgpack.template.Templates;

import tetrodist.event.EventBus;
import tetrodist.field.Field;

public class JabberClient extends Thread implements Closeable {
	private EventBus bus;
	private Socket socket;
	private ExecutorService executor;
	private Field field;

	public JabberClient(EventBus bus, Field field) {
		this.bus = bus;
		this.executor = Executors.newSingleThreadExecutor();
		this.field = field;
	}
	
	public void open(InetAddress addr, int port) throws IOException {
		this.socket = new Socket(addr, port);
		
		OutputStream out = this.socket.getOutputStream();
		OutputStreamWriter outw = new OutputStreamWriter(out);
		BufferedWriter outb = new BufferedWriter(outw);
		final PrintWriter outp = new PrintWriter(outb);

		this.bus.add(ActionListener.class, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String str;
				switch (e.getID()) {
				case EventBus.ROTATE_LEFT:
					str = "z";
					break;
				case EventBus.ROTATE_RIGHT:
					str = "x";
					break;
				case EventBus.MOVE_LEFT:
					str = ",";
					break;
				case EventBus.GRAVITY:
					str = ".";
					break;
				case EventBus.MOVE_RIGHT:
					str = "/";
					break;
				default:
					return;
				}

				outp.println(str);
				outp.flush();

				outp.println("dump");
				outp.flush();
			}
		});
		
		final InputStream in = this.socket.getInputStream();
		
		this.executor.submit(new Runnable() {
			@Override
			public void run() {
				try {
					while (!Thread.interrupted()) {
						MessagePack msgpack = new MessagePack();
						List<List<Integer>> rows = msgpack.read(in, Templates.tList(Templates.tList(Templates.TInteger)));
						for (int i = 0; i < rows.size(); i++) {
							List<Integer> cols = rows.get(i);
							for (int j = 0; j < cols.size(); j++) {
								field.setColorAt(i, j, new Color(cols.get(j)));
							}
						}
						bus.postEvent(new ActionEvent(this, EventBus.EVENT_CHANGE, null));
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public void close() throws IOException {
		this.socket.close();
	}
}