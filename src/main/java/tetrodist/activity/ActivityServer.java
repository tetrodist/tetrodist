package tetrodist.activity;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;

import tetrodist.asset.ProviderImage;
import tetrodist.comm.CommServer;
import tetrodist.controller.ControllerServer;
import tetrodist.discovery.Advertise;
import tetrodist.event.EventBus;
import tetrodist.view.PaintDispatcher;
import tetrodist.view.PaintNext;
import tetrodist.view.PaintPlayingField;
import tetrodist.view.PaintScore;

public class ActivityServer implements Closeable {
	private final Window window;
	private final Advertise advertise;
	private final EventBus bus;
	private final CommServer comm;
	
	private final Insets insets;
	
	private final ControllerServer ctrl;
	
	private final PaintDispatcher paintDispatcher;
	private final PaintPlayingField paintPlayingField;
	private final PaintScore paintScore;
	private final PaintNext paintNext;

	public ActivityServer(Window w, EventBus bus, ProviderImage pi) {
		this.window = w;
		this.bus = bus;
		this.insets = w.getInsets();
		this.ctrl = new ControllerServer(bus, w, pi);

		this.paintDispatcher = new PaintDispatcher(ctrl.localBus, w);

		this.paintPlayingField = new PaintPlayingField(paintDispatcher, ctrl.field, pi);
		this.paintPlayingField.transform = getTransform(48, 16);

		this.paintScore = new PaintScore(paintDispatcher, ctrl.scorer);
		paintScore.transform = getTransform(272, 48);
		paintScore.digitWidth = 5;

		this.paintNext = new PaintNext(paintDispatcher, ctrl.queue, pi);
		paintNext.transform = getTransform(272, 192);
		
		CommServer comm;
		try {
			ServerSocket ss = new ServerSocket(0);
			comm = new CommServer(ss, ctrl.localBus, ctrl.field);
		} catch (IOException e) {
			comm = null;
		}
		this.comm = comm;
		
		if (comm != null) {
			Advertise adv;
			try {
				adv = new Advertise();
				adv.setPort(comm.getServerSocket().getLocalPort());
			} catch (IOException e) {
				adv = null;
			}
			this.advertise = adv;
		} else {
			this.advertise = null;
		}
	}

	public void open(String name) {
		this.ctrl.start();
		
		window.removeAll();
		window.setFocusable(true);
		window.requestFocusInWindow();
		window.createBufferStrategy(2);
		window.setBackground(Color.BLACK);
		BufferStrategy bs = this.window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.clearRect(0, 0, window.getWidth(), window.getHeight());
		bs.show();

		paintDispatcher.addActionListener(paintPlayingField);
		paintDispatcher.addActionListener(paintNext);
		paintDispatcher.addActionListener(paintScore);
		bus.add(ActionListener.class, paintDispatcher);
		
		try {
			comm.open();
		} catch (IOException e) {
		}
		advertise.open();
	}
	
	@Override
	public void close() throws IOException {
		this.advertise.close();
	}
	
	private AffineTransform getTransform(double tx, double ty) {
		AffineTransform xform = ((Graphics2D)window.getGraphics()).getTransform();
		System.out.println(insets);
		xform.translate(insets.left + tx, insets.top + ty);
		return xform;
	}
}
