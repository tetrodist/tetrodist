package tetrodist.activity;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;

import tetrodist.asset.ProviderImage;
import tetrodist.controller.ControllerClient;
import tetrodist.event.EventBus;
import tetrodist.view.PaintDispatcher;
import tetrodist.view.PaintPlayingField;

public class ActivityClient {
	private final Window window;
	private final EventBus bus;
	
	private final Insets insets;
	
	public final ControllerClient ctrl;
	
	private final PaintDispatcher paintDispatcher;
	private final PaintPlayingField paintPlayingField;

	public ActivityClient(Window w, EventBus bus, ProviderImage pi) {
		this.window = w;
		this.bus = bus;
		this.insets = w.getInsets();
		this.ctrl = new ControllerClient(bus, w);

		this.paintDispatcher = new PaintDispatcher(ctrl.localBus, w);

		this.paintPlayingField = new PaintPlayingField(paintDispatcher, ctrl.field, pi);
		this.paintPlayingField.transform = getTransform(48, 16);
	}

	public void open(String name) {
		window.removeAll();
		window.setFocusable(true);
		window.requestFocusInWindow();
		window.createBufferStrategy(2);
		window.setBackground(Color.BLACK);
		BufferStrategy bs = this.window.getBufferStrategy();
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.clearRect(0, 0, window.getWidth(), window.getHeight());
		bs.show();

		paintDispatcher.addActionListener(paintPlayingField);
		bus.add(ActionListener.class, paintDispatcher);
	}

	private AffineTransform getTransform(double tx, double ty) {
		AffineTransform xform = ((Graphics2D)window.getGraphics()).getTransform();
		System.out.println(insets);
		xform.translate(insets.left + tx, insets.top + ty);
		return xform;
	}
}
