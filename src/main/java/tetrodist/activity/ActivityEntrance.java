package tetrodist.activity;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.SynchronousQueue;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import tetrodist.GameEnv;
import tetrodist.asset.ProviderImage;
import tetrodist.discovery.DiscoveryNode;
import tetrodist.discovery.Solicit;

public class ActivityEntrance implements Closeable {
	private final JFrame window;
	private final Solicit solicit;
	private final SynchronousQueue<GameEnv> pipe;
	private Timer timer;
	
	public ActivityEntrance(JFrame window) {
		this.window = window;
		
		Solicit solicit;
		try {
			solicit = new Solicit();
		} catch (IOException e) {
			solicit = null;
			e.printStackTrace();
		}
		this.solicit = solicit;
		
		this.pipe = new SynchronousQueue<>();
	}

	public void open() {
		final Window window = this.window;
		final JTabbedPane p = new JTabbedPane();
		
		p.add(this.createPaneServer(), "Server");
		p.add(this.createPaneClient(), "Client");
		window.add(p);

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				window.setVisible(true);
				window.repaint();
			}
		});
	}
	
	public GameEnv queryGameEnv() throws InterruptedException {
		return pipe.take();
	}
	
	@Override
	public void close() throws IOException {
		this.timer.stop();
	}

	private Timer createTimerSolicit(final Solicit solicit, final DefaultListModel<DiscoveryNode> listServersModel) {
		Timer timer = new Timer(300, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final Set<DiscoveryNode> nodesAdvertised = solicit.getNodes();
				final Set<DiscoveryNode> nodesDisplayed = this.getModelSet(listServersModel);
				
				Set<DiscoveryNode> removing = new HashSet<>(nodesDisplayed);
				removing.removeAll(nodesAdvertised);

				Set<DiscoveryNode> adding = new HashSet<>(nodesAdvertised);
				adding.removeAll(nodesDisplayed);

				for (DiscoveryNode remote : removing) {
					listServersModel.removeElement(remote);
				}

				for (DiscoveryNode remote : adding) {
					listServersModel.addElement(remote);
				}
			}

			private Set<DiscoveryNode> getModelSet(final DefaultListModel<DiscoveryNode> listServersModel) {
				final Enumeration<DiscoveryNode> modelEnum = listServersModel.elements();
				final List<DiscoveryNode> modelList = Collections.list(modelEnum);
				final Set<DiscoveryNode> modelSet = new HashSet<>(modelList);
				return modelSet;
			}
		});
		return timer;
	}

	private JList<DiscoveryNode> createListServer(DefaultListModel<DiscoveryNode> model) {
		JList<DiscoveryNode> listServer = new JList<>();
		listServer.setModel(model);
		return listServer;
	}
	
	private JPanel createPaneClient() {
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints c;

		JPanel p = new JPanel(layout);
		p.setOpaque(false);

		c = new GridBagConstraints();
		c.weightx = 0;
		p.add(new JLabel("Address:"), c);
	
		final JTextField fieldAddr = new JTextField();
		c = new GridBagConstraints();
		c.weightx = 1;
		c.fill = GridBagConstraints.BOTH;
		p.add(fieldAddr, c);

		final JSpinner spinPort = new JSpinner(new SpinnerNumberModel(0, 0, 65535, 1));
		spinPort.setEditor(new JSpinner.NumberEditor(spinPort, "#"));
		c = new GridBagConstraints();
		c.weightx = 0;
		p.add(spinPort, c);
		
		JButton btnClient = new JButton("Connect");
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		p.add(btnClient, c);
		btnClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GameEnv env = new GameEnv();
				env.server = false;
				env.hostname = fieldAddr.getText();
				env.port = (Integer)spinPort.getValue();
				pipe.offer(env);
			}
		});
		
		if (this.solicit != null) {
			final DefaultListModel<DiscoveryNode> listServersModel = new DefaultListModel<>();

			final JList<DiscoveryNode> listServers = createListServer(listServersModel);
			c = new GridBagConstraints();
			c.weightx = 1;
			c.weighty = 1;
			c.fill = GridBagConstraints.BOTH;
			c.gridwidth = GridBagConstraints.REMAINDER;
			c.gridheight = GridBagConstraints.REMAINDER;
			c.gridx = 0;
			c.gridy = 2;
			p.add(listServers, c);
			listServers.addListSelectionListener(new ListSelectionListener() {
				@Override
				public void valueChanged(ListSelectionEvent e) {
					DiscoveryNode selected = listServers.getSelectedValue();
					InetAddress addr = selected.getAddress();
					fieldAddr.setText(addr.getHostAddress());
					spinPort.setValue(selected.getPort());
				}
			});

			Timer timerSolicit = createTimerSolicit(solicit, listServersModel);
			timerSolicit.start();
			timer = timerSolicit;
			
			this.solicit.open();
		}

		return p;
	}
	
	private JPanel createPaneServer() {
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel p = new JPanel(new GridBagLayout());
		p.setOpaque(false);

		c.gridy = 0;
		p.add(new JLabel("Name:"), c);
		
		String name;
		try {
			name = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			name = "";
		}

		final JTextField fieldName = new JTextField(name);
		c.weightx = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		p.add(fieldName, c);


		c.weightx = 0;
		c.fill = GridBagConstraints.NONE;
		c.gridy++;
		p.add(new JLabel("Type:"), c);

		ProviderImage provider = new ProviderImage();
		final JComboBox<ImageIcon> comboType = new JComboBox<>();
		p.add(comboType, c);
		final DefaultComboBoxModel<ImageIcon> combo = new DefaultComboBoxModel<>();
		combo.addElement(provider.getBlockIcon(1));
		combo.addElement(provider.getBlockIcon(2));
		combo.addElement(provider.getBlockIcon(3));
		combo.addElement(provider.getBlockIcon(4));
		combo.addElement(provider.getBlockIcon(5));
		comboType.setModel(combo);

		

		JButton btnOpenGame = new JButton("Open Game");
		c.gridy++;
		c.gridwidth = GridBagConstraints.REMAINDER;
		p.add(btnOpenGame, c);
		btnOpenGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GameEnv env = new GameEnv();
				env.name = fieldName.getText();
				env.server = true;
				env.blockType = comboType.getSelectedIndex() + 1;
				pipe.offer(env);
			}
		});

		return p;
	}
}
