package tetrodist.field;

import java.awt.Color;

import tetrodist.event.EventBus;

public class TetrominoI extends Tetromino {
	protected int[] offsets = {
			// Direction 0
			0, 0, 0, 0, // row
			0, -2, -1, 1, // col

			// Direction 1
			0, -1, 1, 2, // row
			0, 0, 0, 0, // col
	};

	public TetrominoI(EventBus bus, Field pf) {
		super(bus, pf, Color.BLUE);
	}

	@Override
	public int[] getOffsets() {
		return this.offsets;
	}

	public void rotateLeft() {
		this.rotateRight();
	}

	public void rotateRight() {
		int row, col, dir;
		FieldBlockList nextBlocks;

		this.removeFromField();
		try {
			row = this.row;
			col = this.col;
			dir = this.direction ^ 1;

			nextBlocks = this.getMovedBlocks(row, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.direction = dir;
				return;
			}

			nextBlocks = this.getMovedBlocks(row - 1, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.col = col + 2;
				this.direction = dir;
				return;
			}

			nextBlocks = this.getMovedBlocks(row, col - 1, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.col = col - 1;
				this.direction = dir;
				return;
			}

			nextBlocks = this.getMovedBlocks(row, col + 1, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.col = col + 1;
				this.direction = dir;
				return;
			}

			nextBlocks = this.getMovedBlocks(row, col + 2, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.col = col + 2;
				this.direction = dir;
				return;
			}
		} finally {
			this.placeInField();
		}
	}
}
