package tetrodist.field;

import java.awt.Color;

import tetrodist.event.EventBus;

public class TetrominoJ extends Tetromino {
	protected final int[] offsets = {
			// Direction 0
			0, 0, 0, -1, // row
			0, 1, -1, -1, // col

			// Direction 1
			0, 1, -1, -1, // row
			0, 0, 0, 1, // col
			// Direction 2
			0, 0, 0, 1, // row
			0, -1, 1, 1, // col

			// Direction 3
			0, -1, 1, 1, // row
			0, 0,  0, -1, // col
	};

	public TetrominoJ(EventBus bus, Field pf) {
		super(bus, pf, Color.CYAN);
	}

	@Override
	public int[] getOffsets() {
		return this.offsets;
	}

	public void rotateLeft() {
		int row, col, dir;
		FieldBlockList nextBlocks;

		this.removeFromField();
		try {
			row = this.row;
			col = this.col;
			dir = (this.direction - 1) & 0x3;

			nextBlocks = this.getMovedBlocks(row, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.direction = dir;
			}
		} finally {
			this.placeInField();
		}
	}

	public void rotateRight() {
		int row, col, dir;
		FieldBlockList nextBlocks;

		this.removeFromField();
		try {
			row = this.row;
			col = this.col;
			dir = (this.direction + 1) & 0x3;

			nextBlocks = this.getMovedBlocks(row, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.direction = dir;
			}
		} finally {
			this.placeInField();
		}
	}
}
