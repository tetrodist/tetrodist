package tetrodist.field;

import java.awt.Color;
import java.util.ArrayList;

public class FieldBlockList extends ArrayList<FieldBlock> {
	private static final long serialVersionUID = 1465024653522600296L;

	public FieldBlockList(int length) {
		super(length);
	}

	public boolean isAllBlack() {
		for (FieldBlock b : this) {
			if (b.getColor() != Color.BLACK) {
				return false;
			}
		}
		return true;
	}
}
