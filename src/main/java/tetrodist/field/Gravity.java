package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import tetrodist.event.EventBus;
import tetrodist.event.TickEvent;
import tetrodist.event.TickListener;

public class Gravity implements TickListener, ActionListener {
	protected EventBus bus;
	protected int gravityCount = 0;
	
	public Gravity(EventBus bus) {
		this.bus = bus;
		bus.add(TickListener.class, this);
		bus.add(ActionListener.class, this);
	}

	@Override
	public void tick(TickEvent e) {
		this.gravityCount++;
		if (this.gravityCount > 100) {
			ActionEvent ae = new ActionEvent(this, EventBus.GRAVITY, null);
			this.bus.postEvent(ae);
			this.gravityCount = 0;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getID()) {
		case EventBus.GAMEOVER:
			this.bus.remove(TickListener.class, this);
		}
	}
}
