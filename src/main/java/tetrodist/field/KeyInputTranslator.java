package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import tetrodist.event.EventBus;
import tetrodist.event.TickEvent;
import tetrodist.event.TickListener;

public class KeyInputTranslator {
	protected Map<Integer, Integer> eventIdTable;
	protected Set<Integer> oneshotKeys;
	protected Set<Integer> pressedKeys;
	protected Set<Integer> prohibitedKeys;
	protected int repeat;

	public KeyInputTranslator(final EventBus bus) {
		this.eventIdTable = new HashMap<Integer, Integer>();
		this.oneshotKeys = new HashSet<Integer>();
		this.prohibitedKeys = new HashSet<Integer>();
		this.pressedKeys = new LinkedHashSet<Integer>();
		this.pressedKeys = Collections.synchronizedSet(this.pressedKeys);

		bus.add(KeyListener.class, new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (!prohibitedKeys.contains(e.getKeyCode())) {
					if (pressedKeys.add(e.getKeyCode())) {
						Integer id = KeyInputTranslator.this.eventIdTable.get(e.getKeyCode());
						if (id != null) {
							ActionEvent ae = new ActionEvent(this, id, "");
							bus.postEvent(ae);
						}
						repeat = -30;
					}
				}
			}
			@Override
			public void keyReleased(KeyEvent e) {
				KeyInputTranslator.this.pressedKeys.remove(e.getKeyCode());
				KeyInputTranslator.this.prohibitedKeys.remove(e.getKeyCode());
			}
		});

		bus.add(TickListener.class, new TickListener() {
			@Override
			public void tick(TickEvent e) {
				ArrayList<Integer> toremove = new ArrayList<Integer>();
				if (repeat < 15) {
					repeat++;
					return;
				}
				
				int dispatchId = -1;
				for (int keyCode : KeyInputTranslator.this.pressedKeys) {
					Integer id = KeyInputTranslator.this.eventIdTable.get(keyCode);
					if (id != null) {
						dispatchId = id;
					}
					
					if (KeyInputTranslator.this.oneshotKeys.contains(keyCode)) {
						toremove.add(keyCode);
						KeyInputTranslator.this.prohibitedKeys.add(keyCode);
					}
				}
				if (dispatchId != -1) {
					bus.postEvent(new ActionEvent(this, dispatchId, null));
				}
				repeat = 0;
				KeyInputTranslator.this.pressedKeys.removeAll(toremove);
			}
		});
		
		bus.add(ActionListener.class, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (e.getID()) {
				case EventBus.GAMEOVER:
					System.out.println("GAMEOVER");
					bus.remove(ActionListener.class, this);
					break;
				}
			}
		});
	}

	public void registerRepeatKey(int keyCode, int eventId) {
		this.eventIdTable.put(keyCode, eventId);
	}

	public void registerOneshotKey(int keyCode, int eventId) {
		this.oneshotKeys.add(keyCode);
		this.eventIdTable.put(keyCode, eventId);
	}

	public void unregisterAllKeys() {
		this.oneshotKeys.clear();
		this.eventIdTable.clear();
	}
}
