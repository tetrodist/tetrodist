package tetrodist.field;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FieldLine {
	protected final List<Color> colors;
	protected final List<FieldBlock> blocks;
	protected final Field field;

	public FieldLine(Field field, int width) {
		this.field = field;

		ArrayList<FieldBlock> blocks = new ArrayList<FieldBlock>(width);
		this.colors = new ArrayList<Color>(width);

		for (int col = 0; col < width; col++) {
			blocks.add(new FieldBlock(this, col));
			this.colors.add(Color.BLACK);
		}

		this.blocks = Collections.unmodifiableList(blocks);
	}

	public List<FieldBlock> getBlocks() {
		return this.blocks;
	}

	public int getRow() {
		return this.field.getLines().indexOf(this);
	}

	public int getWidth() {
		return this.field.getWidth();
	}

	public boolean isFilled() {
		return !colors.contains(Color.BLACK);
	}

	public Color getColorAt(int col) {
		return colors.get(col);
	}

	public Color setColorAt(int col, Color color) {
		return colors.set(col, color);
	}

	public void setColorAll(Color color) {
		Collections.fill(this.colors, color);
	}

	public FieldBlock getBlockAt(int col) {
		return this.blocks.get(col);
	}
}