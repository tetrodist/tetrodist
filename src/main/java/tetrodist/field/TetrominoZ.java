package tetrodist.field;

import java.awt.Color;

import tetrodist.event.EventBus;

public class TetrominoZ extends Tetromino {
	protected final int[] offsets = {
			// Direction 0
			0, 0, -1, -1, // row
			0, 1, 0, -1, // col

			// Direction 1
			0, -1, 0, 1, // row
			0, 0, -1, -1, // col
	};

	public TetrominoZ(EventBus bus, Field pf) {
		super(bus, pf, Color.RED);
	}

	@Override
	public int[] getOffsets() {
		return this.offsets;
	}

	public void rotateLeft() {
		this.rotateRight();
	}

	public void rotateRight() {
		int row, col, dir;
		FieldBlockList nextBlocks;

		this.removeFromField();
		try {
			row = this.row;
			col = this.col;
			dir = this.direction ^ 1;
			nextBlocks = this.getMovedBlocks(row, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.direction = dir;
			}
		} finally {
			this.placeInField();
		}
	}
}
