package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import tetrodist.event.EventBus;

public class TetroFixer implements ActionListener {
	protected EventBus bus;
	protected Field field;

	public TetroFixer(EventBus bus, Field field) {
		this.bus = bus;
		bus.add(ActionListener.class, this);
		this.field = field;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getID()) {
		case EventBus.TETROMINO_FIXING:
			Tetromino tetro = (Tetromino) e.getSource();
			Field pf = tetro.getPlayingField();
			pf.removeTetromino(tetro);

			ActionEvent ae = new ActionEvent(this, EventBus.TETROMINO_FIXED, null);
			this.bus.postEvent(ae);
		}
	}

}
