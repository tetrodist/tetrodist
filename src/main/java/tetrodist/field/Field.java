package tetrodist.field;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import tetrodist.event.EventBus;

public class Field {
	public Color outOfField = Color.WHITE;
	public Color blank = Color.BLACK;
	public final FieldLine outOfFieldLine;
	public final FieldBlock outOfFieldBlock;

	private EventBus hub;
	private List<Tetromino> tetros;
	public final int width;
	public final int height;
	private List<FieldLine> lines;

	public Field(EventBus hub, int height, int width) {
		this.hub = hub;
		this.tetros = new ArrayList<Tetromino>();
		this.width = width;
		this.height = height;
		this.outOfFieldLine = new FieldLine(this, width);
		this.outOfFieldBlock = new FieldBlock(this.outOfFieldLine, 0);
		this.outOfFieldBlock.setColor(this.outOfField);

		this.lines = new ArrayList<FieldLine>(height);
		for (int row = 0; row < height; row++) {
			this.lines.add(new FieldLine(this, width));
		}
	}
	
	public void eraseLines(Collection<FieldLine> lines) {
		this.lines.removeAll(lines);
		this.lines.addAll(0, lines);
		
		for (FieldLine line : lines) {
			line.setColorAll(Color.BLACK);
		}
	}

	public Iterable<Tetromino> getTetrominos() {
		return this.tetros;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public FieldBlock getBlockAt(int row, int col) {
		if (this.isWithinField(row, col)) {
			return this.lines.get(row).getBlockAt(col);
		} else {
			return this.outOfFieldBlock;
		}
	}

	public Color getColorAt(int row, int col) {
		if (this.isWithinField(row, col)) {
			FieldLine line = this.lines.get(row);
			return line.getColorAt(col);
		}

		return this.outOfField;
	}

	public void setColorAt(int row, int col, Color color) {
		if (this.isWithinField(row, col)) {
			FieldLine lines = this.lines.get(row);
			lines.setColorAt(col, color);
		}
	}
	
	public List<FieldLine> getLines() {
		return Collections.unmodifiableList(this.lines);
	}

	public boolean isFilled(int row) {
		FieldLine line = this.lines.get(row);
		return line.isFilled();
	}

	public boolean isBlank(int row, int col) {
		return this.isWithinField(row, col)
				&& this.getColorAt(row, col) == this.blank;
	}

	public boolean isWithinField(int row, int col) {
		return this.isWithinRow(row) && this.isWithinColumn(col);
	}

	public boolean isWithinRow(int row) {
		return row >= 0 && row < this.height;
	}

	public boolean isWithinColumn(int col) {
		return col >= 0 && col < this.width;
	}

	public void addTetromino(Tetromino tetro) {
		this.tetros.add(tetro);
		this.hub.add(ActionListener.class, tetro);
	}

	public void removeTetromino(Tetromino tetro) {
		this.hub.remove(ActionListener.class, tetro);
		this.tetros.remove(tetro);
	}
}
