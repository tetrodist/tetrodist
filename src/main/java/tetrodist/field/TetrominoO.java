package tetrodist.field;

import java.awt.Color;

import tetrodist.event.EventBus;

public class TetrominoO extends Tetromino {
	protected int[] offsets = {
			// Direction 0
			0, 0, 1, 1, // row
			0, 1, 0, 1, // col
	};

	public TetrominoO(EventBus bus, Field field) {
		super(bus, field, Color.YELLOW);
	}

	@Override
	public int[] getOffsets() {
		return this.offsets;
	}

	@Override
	public void rotateLeft() {
	}

	@Override
	public void rotateRight() {
	}
}
