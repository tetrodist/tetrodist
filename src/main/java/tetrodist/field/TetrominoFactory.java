package tetrodist.field;

import tetrodist.event.EventBus;

public class TetrominoFactory {
	public static final int I = 0;
	public static final int O = 1;
	public static final int S = 2;
	public static final int Z = 3;
	public static final int T = 4;
	public static final int L = 5;
	public static final int J = 6;
	
	protected EventBus bus;
	protected Field field;

	public TetrominoFactory(EventBus bus, Field field) {
		this.bus = bus;
		this.field = field;
	}
	
	public Tetromino newTetromino(int type) {
		switch (type) {
		case TetrominoFactory.I:
			return new TetrominoI(this.bus, field);
		case TetrominoFactory.O:
			return new TetrominoO(this.bus, field);
		case TetrominoFactory.S:
			return new TetrominoS(this.bus, field);
		case TetrominoFactory.Z:
			return new TetrominoZ(this.bus, field);
		case TetrominoFactory.T:
			return new TetrominoT(this.bus, field);
		case TetrominoFactory.L:
			return new TetrominoL(this.bus, field);
		case TetrominoFactory.J:
			return new TetrominoJ(this.bus, field);
		}

		throw new IllegalArgumentException();
	}
}
