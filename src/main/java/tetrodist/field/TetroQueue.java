package tetrodist.field;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;

public class TetroQueue extends ArrayDeque<Tetromino> {
	private static final long serialVersionUID = 1327518824488836127L;

	protected TetrominoFactory factory;

	private ArrayList<Tetromino> bag;

	public TetroQueue(TetrominoFactory factory) {
		this.factory = factory;
		this.bag = new ArrayList<Tetromino>();
	}

	public void addBag() {
		this.bag.clear();

		for (int i = 0; i < 7; i++) {
			this.bag.add(this.factory.newTetromino(i));
		}

		Collections.shuffle(this.bag);

		this.addAll(this.bag);
	}

	public void ensureSize(int size) {
		while (this.size() < size) {
			this.addBag();
		}
	}

	@Override
	public Tetromino poll() {
		this.ensureSize(3);
		return this.pollFirst();
	}
}
