package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Closeable;
import java.io.IOException;

import tetrodist.event.EventBus;
import tetrodist.event.TickEvent;
import tetrodist.event.TickListener;

public class TetroInjector implements ActionListener, TickListener, Closeable {
	protected EventBus bus;
	protected Field field;
	protected TetroQueue queue;

	private int counter;

	public TetroInjector(EventBus bus, Field field, TetroQueue queue) {
		this.bus = bus;
		this.field = field;
		this.queue = queue;
		this.counter = -1;
	}
	
	public void open() {
		bus.add(ActionListener.class, this);
		bus.add(TickListener.class, this);
	}

	@Override
	public void close() throws IOException {
		bus.remove(ActionListener.class, this);
		bus.remove(TickListener.class, this);
	}

	public void inject() {
		if (!this.field.isBlank(1, 4)) {
			this.bus.postEvent(new ActionEvent(this, EventBus.GAMEOVER, null));
			System.out.println("GAMEOVER");
			return;
		}
		Tetromino tetro = this.queue.poll();
		this.field.addTetromino(tetro);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getID()) {
		case EventBus.INJECTING:
		case EventBus.TETROMINO_FIXED:
			this.counter = 0;
		}
	}

	@Override
	public void tick(TickEvent e) {
		if (this.counter > 20) {
			this.inject();
			this.counter = -1;
		} else if (this.counter >= 0) {
			this.counter++;
		}
	}
}
