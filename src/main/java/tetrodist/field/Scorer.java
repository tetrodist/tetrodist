package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import tetrodist.event.EventBus;

public class Scorer implements ActionListener {
	protected EventBus bus;
	protected Field field;
	protected int score = 0;
	protected Map<Integer, Integer> scoreTable;

	public Scorer(EventBus bus) {
		this.bus = bus;
		bus.add(ActionListener.class, this);
		this.scoreTable = new HashMap<Integer, Integer>();
		this.scoreTable.put(EventBus.ERASE_SINGLE, 40);
		this.scoreTable.put(EventBus.ERASE_DOUBLE, 100);
		this.scoreTable.put(EventBus.ERASE_TRIPLE, 300);
		this.scoreTable.put(EventBus.ERASE_TETRIS, 1200);
	}

	public int getScore() {
		return score;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Integer i = this.scoreTable.get(e.getID());
		if (i != null) {
			this.score += i;
		}
	}
}
