package tetrodist.field;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import tetrodist.event.EventBus;

public class LineEraser implements ActionListener {
	protected EventBus bus;
	protected Field field;
	protected int score = 0;

	private ArrayList<FieldLine> erasingLines;

	public LineEraser(EventBus bus, Field field) {
		this.bus = bus;
		bus.add(ActionListener.class, this);
		this.field = field;
		this.erasingLines = new ArrayList<FieldLine>();
	}
	
	public void dispatchErase(int count) {
		ActionEvent e;

		switch (count) {
		case 1:
			e = new ActionEvent(this, EventBus.ERASE_SINGLE, null);
			break;
		case 2:
			e = new ActionEvent(this, EventBus.ERASE_DOUBLE, null);
			break;
		case 3:
			e = new ActionEvent(this, EventBus.ERASE_TRIPLE, null);
			break;
		case 4:
			e = new ActionEvent(this, EventBus.ERASE_TETRIS, null);
			break;
		default:
			return;
		}
		
		this.bus.postEvent(e);
	}
	
	public void erase() {
		this.erasingLines.clear();

		for (FieldLine line : this.field.getLines()) {
			if (line.isFilled()) {
				this.erasingLines.add(line);
			}
		}

		this.dispatchErase(this.erasingLines.size());

		this.field.eraseLines(this.erasingLines);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getID()) {
		case EventBus.TETROMINO_FIXED:
			this.erase();
		}
	}
}
