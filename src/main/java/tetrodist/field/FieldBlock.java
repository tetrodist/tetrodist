package tetrodist.field;

import java.awt.Color;

public class FieldBlock {
	public final FieldLine line;
	public final int col;

	public FieldBlock(FieldLine line, int col) {
		this.line = line;
		this.col = col;
	}

	public int getRow() {
		return this.line.getRow();
	}

	public int getCol() {
		return this.line.getBlocks().indexOf(this);
	}

	public Color getColor() {
		return this.line.getColorAt(this.col);
	}

	public void setColor(Color color) {
		this.line.setColorAt(this.col, color);
	}
}
