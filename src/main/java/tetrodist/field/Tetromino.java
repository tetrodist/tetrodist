package tetrodist.field;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import tetrodist.event.EventBus;

public abstract class Tetromino implements ActionListener {
	public static final int EVENT_CHANGE = EventBus.EVENT_CHANGE;

	protected int numBlocks = 4;

	protected int direction;
	protected FieldBlockList blocks;
	protected FieldBlockList shadow;
	protected Field field;
	protected int row = 0;
	protected int col = 4;
	protected Color color;
	protected EventBus bus;
	protected int fixWait = 1;

	public Tetromino(EventBus bus, Field field, Color c) {
		this.bus = bus;
		this.field = field;
		this.blocks = new FieldBlockList(4);
		this.shadow = new FieldBlockList(4);
		this.color = c;
	}

	public int getDirection() {
		return this.direction;
	}

	public Field getPlayingField() {
		return this.field;
	}

	protected void removeFromField() {
		for (FieldBlock b : this.blocks) {
			b.setColor(Color.BLACK);
		}
	}

	protected void placeInField() {
		for (FieldBlock b : this.blocks) {
			b.setColor(this.color);
		}
		this.dispatchChange();
	}

	public FieldBlock[] getBlocks() {
		return blocks.toArray(new FieldBlock[0]);
	}

	public abstract int[] getOffsets();

	public FieldBlockList getMovedBlocks(int row, int col, int dir) {
		int index = dir * this.numBlocks * 2;

		FieldBlockList blocks = new FieldBlockList(this.numBlocks);
		int[] offsets = this.getOffsets();

		for (int i = 0; i < 4; i++) {
			int rowOff = offsets[index + i];
			int colOff = offsets[index + this.numBlocks + i];

			FieldBlock b = this.field.getBlockAt(row + rowOff, col + colOff);

			blocks.add(b);
		}

		return blocks;
	}

	protected void moveHorizontal(int offset) {
		int row, col, dir;
		FieldBlockList nextBlocks = this.blocks;

		this.removeFromField();

		try {
			row = this.row;
			col = this.col + offset;
			dir = this.direction;
			nextBlocks = this.getMovedBlocks(row, col, dir);

			if (nextBlocks.isAllBlack()) {
				this.blocks = nextBlocks;
				this.col = col;
				return;
			}
		} finally {
			this.placeInField();
		}
	}

	public void fall() {
		int row = this.row;
		int col = this.col;
		int dir = this.direction;

		FieldBlockList nextBlocks = this.getMovedBlocks(row, col, dir);

		this.removeFromField();
		while (nextBlocks.isAllBlack()) {
			nextBlocks = this.getMovedBlocks(++row, col, dir);
		}

		this.blocks = this.getMovedBlocks(row - 1, col, dir);
		this.row = row - 1;

		this.placeInField();
		this.dispatchFixing();
	}

	public void gravity() {
		int row = this.row + 1;
		int col = this.col;
		int dir = this.direction;

		FieldBlockList nextBlocks = this.getMovedBlocks(row, col, dir);

		this.removeFromField();

		if (nextBlocks.isAllBlack()) {
			this.blocks = nextBlocks;
			this.row = row;
			this.placeInField();
		} else {
			this.placeInField();
			if (this.fixWait < 0) {
				this.dispatchFixing();
			} else {
				this.fixWait--;
			}
		}

	}

	public abstract void rotateLeft();

	public abstract void rotateRight();

	public void dispatchChange() {
		ActionEvent e = new ActionEvent(this, EventBus.EVENT_CHANGE, null);
		this.bus.postEvent(e);
	}

	public void dispatchFixing() {
		ActionEvent e = new ActionEvent(this, EventBus.TETROMINO_FIXING, null);
		this.bus.postEvent(e);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getID()) {
		case EventBus.GRAVITY:
			this.gravity();
			break;
		case EventBus.MOVE_LEFT:
			this.moveLeft();
			break;
		case EventBus.MOVE_RIGHT:
			this.moveRight();
			break;
		case EventBus.ROTATE_LEFT:
			this.rotateLeft();
			break;
		case EventBus.ROTATE_RIGHT:
			this.rotateRight();
			break;
		case EventBus.FALL:
			this.fall();
			break;
		}
	}

	public void moveLeft() {
		this.moveHorizontal(-1);
	}

	public void moveRight() {
		this.moveHorizontal(1);
	}
}
